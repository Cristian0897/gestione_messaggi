package com.messaggi.gestione_messaggi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "messaggi" )
public class Message {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_messaggio")
	private int id_messaggio ;
	
	@Column( name = "messaggio" )
	private String messaggio ;
	
	@Column( name = "utente" )
	private String utente ;
	
	@Column( name = "data_ora_messaggio")
	private String data_ora_messaggio ;

	public int getId_messaggio() {
		return id_messaggio;
	}

	public void setId_messaggio(int id_messaggio) {
		this.id_messaggio = id_messaggio;
	}

	public String getMessaggio() {
		return messaggio;
	}

	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}

	public String getUtente() {
		return utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}

	public String getData_ora_messaggio() {
		return data_ora_messaggio;
	}

	public void setData_ora_messaggio(String data_ora_messaggio) {
		this.data_ora_messaggio = data_ora_messaggio;
	}
	
}
