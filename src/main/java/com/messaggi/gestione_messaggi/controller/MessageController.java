package com.messaggi.gestione_messaggi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.messaggi.gestione_messaggi.models.Message;
import com.messaggi.gestione_messaggi.service.MessageService;

@RestController
@RequestMapping("/message")
@CrossOrigin(origins="*", allowedHeaders="*")
public class MessageController {

	@Autowired
	private MessageService service ;
	
	@GetMapping("/test")
	public String test() {
		return "Hello World!" ;
	}
	
	@PostMapping("/inserisciMessaggio")
	public Message aggiungiMessaggio( @RequestBody Message objMessage ) {
		
		return service.saveMessage(objMessage) ;
		
	}
	
	@GetMapping("/messaggio/{id_messaggio}")
	public Message trovaMessaggio( @PathVariable int id_messaggio ) {
		
		return service.findById(id_messaggio) ;
		
	}
	
	@GetMapping("/messaggio/tutti")
	public List<Message> tuttiMessaggi(){
		
		return service.findAllMessages() ;
		
	}
	
	@DeleteMapping("/delete/{id_messaggio}")
	public boolean eliminaMessaggio( @PathVariable int id_messaggio) {
		
		return service.deleteMessage(id_messaggio) ;
		
	}
	
}
