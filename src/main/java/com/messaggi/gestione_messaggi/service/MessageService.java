package com.messaggi.gestione_messaggi.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.messaggi.gestione_messaggi.models.Message;

@Service
public class MessageService {
	
	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public Message saveMessage( Message objMessage ) {
		
		Message temp = new Message() ;
		temp.setMessaggio(objMessage.getMessaggio()) ;
		temp.setUtente(objMessage.getUtente()) ;
		temp.setData_ora_messaggio(objMessage.getData_ora_messaggio()) ;
		
		Session sessione = getSessione() ;
		
		try {
			
			sessione.save(temp) ;
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
		}
		
		return temp ;
		
	}
	
	public Message findById( int varId ) {
		
		Session sessione = getSessione() ;
		
		return( Message )sessione
				.createCriteria(Message.class)
				.add(Restrictions.eqOrIsNull("id_messaggio", varId))
				.uniqueResult();		
	}
	
	public List<Message> findAllMessages(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Message.class).list() ;
		
	}
	
	
	@Transactional
	public boolean deleteMessage( int varId ) {
		
		Session sessione = getSessione() ;
		
		try {
			
			Message temp = sessione.load( Message.class, varId ) ;
			
			sessione.delete(temp);
			sessione.flush();
			
			return true ;
			
		} catch(Exception e) {
			
			System.out.println(e.getMessage());
			
			
		}
		
		return false ;
		
	}

}
